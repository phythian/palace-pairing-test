RSpec.describe Api::V1::BasketsController do
  describe 'GET index' do
    subject { get :index }

    let(:expected_response) {
      { hello: 'Tom' }.to_json
    }

    it 'returns a hello Tom' do
      subject
      expect(response.body).to eql(expected_response)
    end
  end
end
