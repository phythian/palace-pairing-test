Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :baskets
      resources :products
    end
  end
end
