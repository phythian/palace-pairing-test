class Product < ApplicationRecord
  validates :sku, presence: true, uniqueness: true
  validates :unit_price, presence: true, numericality: true
  validates :discount_quantity, numericality: true
  validates :discount_price, numericality: true

  def as_palace_hash
    {
      sku: sku,
      unit_price: unit_price,
      special_price: {
        discounted_price: discount_price,
        quantity_required: discount_quantity
      }
    }
  end
end
