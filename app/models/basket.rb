class Basket < ApplicationRecord
  serialize :contents, Array

  def total
    {
      total_cost: palace_coding_test_total,
      items: contents,
      discount_applied: "You saved #{discount_applied}!"
    }
  end

  private

  def discount_applied
    undiscounted_price - palace_coding_test_total
  end

  def undiscounted_price
    contents.map do |item|
      Product.find_by_sku(item)
    end.sum(&:unit_price)
  end

  def palace_coding_test_total
    @palace_coding_test_total ||= contents.each do |item|
      palace_basket.add(item)
    end

    palace_basket.total
  end

  def palace_basket
    @palace_basket ||= PalaceCodingTest::Basket.new(all_products)
  end

  def all_products
    Product.all.map(&:as_palace_hash)
  end
end
