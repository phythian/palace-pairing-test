class Api::V1::BasketsController < ApplicationController
  before_action :find_basket, only: [:show, :update]
  def index
    render json: { hello: 'Tom' }
  end

  def show
    render json: @basket.total
  end

  def create
    @basket = Basket.new

    if @basket.save
      render json: @basket
    else
      render json: { error: 'SOMETHING WENT WRONG' }
    end
  end

  def update
    @basket.contents << sku

    if @basket.save
      render json: @basket
    else
      render json: { error: 'SOMETHING WENT WRONG' }
    end
  end

  private

  def find_basket
    @basket = Basket.find(basket_id)
  end

  def basket_id
    permitted_params.fetch(:id)
  end

  def sku
    permitted_params.fetch(:sku)
  end

  def permitted_params
    params.permit(
      :id,
      :sku
    )
  end
end
