class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :sku, uniqueness: true
      t.integer :unit_price
      t.integer :discount_quantity
      t.integer :discount_price
    end
  end
end
